import turtle
import random


class Rosace:

    def __init__(self, table=2, modulo=360, rayon=200, couleur='black'):
        self.table = table
        self.modulo = modulo
        self.rayon = rayon
        self.couleur = couleur
        self.points = []
        self.tortue = turtle.Turtle()
    
    def settings(self):
        self.tortue.ht()
        self.tortue.screen.colormode(255)
        self.tortue.color(self.couleur)
        self.tortue.screen.tracer(400)
    
    def loop(self):
        self.tortue.screen.update()
        self.tortue.screen.mainloop()

    def circle(self):
        self.aller(0, -self.rayon/2)
        angle = 360 // self.modulo
        for _ in range(self.modulo):
            self.points.append(self.tortue.pos())
            self.tortue.circle(self.rayon, angle)

    def aller(self, x, y, trace=False):
        if not trace:
            self.tortue.up()
        self.tortue.goto(x, y)
        self.tortue.down()

    def segments(self):
        for i in range(1, self.modulo):
            x, y = self.points[i]
            self.aller(x, y)
            n = (self.table * i) % self.modulo
            x, y = self.points[n]
            self.aller(x, y, trace=True)

    def draw(self):
        self.circle()
        self.segments()


couleur = random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
table = random.randint(2, 200)
rosace = Rosace(table, rayon=200, couleur=couleur)
rosace.settings()
rosace.draw()
# rosace.circle()

rosace.loop()