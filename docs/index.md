![logo](assets/images/logo_rosace.svg) 

# Python et les Objets pour Modéliser et Programmer

Commencer par apprendre à décrire les choses, avec des objets et des phrases puis préciser des actions. Bref faire une initiation _descendante_ de la programmation en utilisant la programmation orientée objet et le langage Python.

![Jean Piaget](assets/images/jean_piaget.png){ align=right } Cette initiation est fondée sur les objets et sur la **pratique**. Expérimenter soi-même les codes, les exemples pour mieux s'approprier les concepts de la programmation et de la résolution de problème. Ce mode d'apprentissage n'est pas nouveau, il date des années 1920, s'appelle le [constructivisme](https://fr.wikipedia.org/wiki/Constructivisme_(psychologie)) et nous vient de Jean Piaget (en autre).

![Seymour Papert](assets/images/seymour_papert.jpg){align=left} [Seymour Papert](https://fr.wikipedia.org/wiki/Seymour_Papert), mathématicien, informaticien, éducateur, psychologue est l'un des chercheurs qui a le mieux compris les idées de Piaget (de l'avis de Piaget lui-même). Il est l'inventeur du langage [LOGO](https://fr.wikipedia.org/wiki/Logo_(langage)), langage procédural réflexif et orienté objet inspiré par LISP mais mieux adapté à une démarche de pédagogie active.

Pour rendre la programmation LOGO accessible à des enfants, S. Papert intègre un environnement graphique : la tortue LOGO. Sorte de petit robot que l'on peut commander via des instructions simples et qui évolue dans une fenêtre graphique. Cette tortue sera un véritable succès éclipsant même le fait que LOGO est avant tout un langage de haut niveau non exclusivement destiné aux activités artistiques d'enfants de 9-10 ans.

Néanmoins, c'est bien cette tortue LOGO qui va constituer notre point de départ. Alors pas elle directement mais son adaptation par le langage Python : le module `turtle` (signifiant tortue en anglais). Dès lors, puisqu'on nous souhaitons un apprentissage actif, nous allons commencer par le plus pénible : l'installation de quelques outils indispensables.

