# Factoriser du code

- Eviter d'écrire plusieurs fois les mêmes instructions ou des instructions similaires
- Regrouper dans une fonction les instructions qui résolvent une tâche bien précise


## Parcourir des éléments

!!! example "Exemples"
    - Lorsqu'un sportif effectue une répétition de mouvements il n'est pas rare qu'il compte dans sa tête pour savoir combien il en a fait. Compter c'est énuméré un à un les entiers 1, 2, etc.

    - Lorsqu'nous épelons un mot, nous énumèrons les caractères un à un.

    - Lorsque nous distribuons un jeu de cartes, nous parcourons une à une les cartes du jeu (pour les ditribuer).

En programmation, le concept d'itération est fondamental. De nombreux objets de Python sont **itérables** :

- `range(5)` est un itérable sur les entiers de 0 à 4
- `'hello'` est un objet `str` (le début du mot _string_ qui signifie chaîne de caractères en anglais) sur lequel il est possible d'itérer
- l'objet `list` est un objet qui peut contenir plusieurs objets (comme des cartes) et sur lequel on peut itérer

L'instruction Python permettant d'itérer sur un itérable est la boucle `for`. Voici, sur l'exemple du `range` comment elle s'utilise :

```python
for ma_variable in range(5):
    # ici le traitement qu'on souhaite effectuer
``` 

Au premier passage, `ma_variable` référence le premier entier distribué par l'objet `range` : 0. L'interprète effectue les instructions qui se trouvent dans le corps de la boucle. Puis il repasse par le `for`, et `ma_variable` récupère l'entier suivant c'est-à-dire 1 et effectue les instructions dans le corps de la boucle, etc. Jusqu'à l'entier 5 avec lequel le corps de la boucle n'est pas effectué.

!!! note
    À tester (on a remplacé `ma_variable` par `i` plus court):
    
    ```python
    for i in range(5):
        print('on passe dans la boucle, i vaut :', i)
    print('on est sorti, i vaut :', i)
    ```

    ```python
    print("bonjour, s'écrit :")
    for lettre in 'bonjour':
        print(lettre)
    ``` 

Dans les exemples ci-dessus dans le corps de la boucle nous nous servons de la valeur courante de la variable. La valeur **courante** signifie celle distribuée par l'itérable au moment du passage dans la boucle. Mais parfois, nous n'avons pas besoin de cette valeur. C'est le cas par exemple si nous comptons le nombre de passage dans la boucle. 

Ci-dessous un exemple où nous affichons 10 fois `'hello'` :

!!! example "Exemple"

    ```python
    for _ in range(10):
        print('hello')
    ```

`range(10)` distribue les entiers de 0 à 9 (ce qui fait bien 10 entiers) et nous devons, dans la syntaxe de la boucle `for` fournir une variable pour accueillir ces entiers. Comme nous n'utilisons pas les entiers, nous utilisons une sorte de variable _anonyme_ : `_` le caractère souligné, appelé aussi _underscore_ en anglais.

!!! info
    L'utilisation de la variable `_` n'est pas une obligation : vous pouvez, même si elle n'est pas utilisée donner un nom à votre variable.

    
## Retour aux méthodes

Maintenant que nous avons un moyen de répéter les instructions, terminons notre méthode pour dessiner. Juste un mot sur le positionnement. Il s'agit d'aller **sans laisser de trace** au point de coordonnées définies par la propriété `pos`. Définissons une méthode `se_positionne()` :

```python
def se_positionne(self):
    x, y = self.pos
    self.t.up()
    self.t.lt(180)
    self.fd(x)
    self.lt(90)
    self.fd(y)
    self.lt(90)
```

```python
def se_dessine(self):
    self.se_positionne()
    for _ in range(4):
        self.t.fd(self.cote)
        self.t.lt(90)
```
Et voici à quoi ressemble notre nouveau programme `carre.py`:

??? success "carre.py"
    
    ```python
    import turtle

    class Carre:

        def __init__(self, x, y, cote, tortue):
            self.pos = x, y
            self.cote = cote
            self.t = tortue

        def se_positionne(self):
            x, y = self.pos
            self.t.up()
            self.t.lt(180)
            self.t.fd(x)
            self.t.lt(90)
            self.t.fd(y)
            self.t.lt(90)
            self.t.down()

        def se_dessine(self):
            self.se_positionne()
            for _ in range(4):
                self.t.fd(self.cote)
                self.t.lt(90)
    
    # Le programme principal

    carre = Carre(0, 0, 150, turtle.Turtle())
    carre.se_dessine()

    turtle.mainloop()
    ```

Ainsi si nous voulons créer un autre carré, en 20, 50 et taille 200 :

```python
autre_carre = Carre(20, 50, 200, turtle.Turtle())
```

Mais une petite minute... à chaque carré créé, nous créons une tortue ? Une tortue peut certainement dessiner plusieurs carrés et ça semble un peu surdimensionné de devoir en créer une par carré. Voyons comment n'avoir qu'une tortue pour tous nos carrés.


!!! warning "Éléments de syntaxe à ne pas oublier"
    - Ne pas oublier les `:` lors des définitions de méthodes ou de classes et dans les boucles. Ces lignes s'appellent des **entêtes** et indiquent qu'il y aura un **corps** 
    - Ne pas oublier l'indentation du bloc d'instructions qui arrive après une ligne d'entête.
    - Ne pas oublier `turtle.mainloop()` à la fin de votre programme manipulant des _turtle_ 


## Un attribut de classe

La propriété `t` qui est un objet Turtle n'a pas besoin d'être différente pour chaque carré créé. Voici une solution :

```python
class Carre:

    tortue = turtle.Turtle()

    def __init__(self, x, y, cote):
        self.pos = x, y
        self.cote = cote
        self.t = Carre.tortue
    
    ...
```

Nous avons créé une `tortue` au niveau de la classe. Chaque instance définit un _alias_ c'est-à-dire une nouvelle référence vers le même unique objet Turtle. Vous vous souvenez, comme `caro` était un _alias_ de `caroline`.

!!! note
    Lorsqu'on définit une propriété `prop` à l'extérieure du constructeur d'une classe `A`, il s'agit d'une propriété de classe et s'utilise ainsi : `A.prop`

 