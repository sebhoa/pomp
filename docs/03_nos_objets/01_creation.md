# Notre premier objet

Notre interprète interactif ne va plus être suffisamment souple d'emploi pour la suite. Il est temps de lancer l'éditeur de texte et d'y construire nos programmes.

## Premier programme


1. Lancez votre éditeur et dans une nouvelle fenêtre tapez le code suivant :
   
    ```python
    print('Ceci est mon premier programme')
    ```

2. Enregistrez le fichier en le nommant `prog.py`, dans un répertoire que vous avez bien pris soin de repérer.
3. Pour exécuter votre programme, dans le Terminal tapez ceci :

    ```bash
    python prog.py
    ```
    Le résultat devrait être :

    ```
    Ceci est mon premier programme
    ``` 

    Si tel est le cas, vous êtez prêts à créer vos objets.


## Objet `Carre` 

Nous allons créer un programme `carre.py` contenant les lignes suivantes :

```python
import turtle

class Carre:

    def __init__(self, x, y, cote, tortue):
        self.pos = x, y
        self.cote = cote
        self.t = tortue
```

Avant d'aller plus loin commentons chacune des lignes :

- `class Carre:`  (ne  pas oublier le `:` à la fin de la ligne) permet de créer un nouvel Objet. `Turtle` est aussi une classe, définie dans le module `turtle`. Grâce à la classe, nous sommes en mesure de créer des copies différentes d'objet, nous parlons d'**instance**. Ainsi, Caroline était une instance de `Turtle`.

    La suite est ce qu'on appelle le **corps** de la classe. Notez que ce corps est décalé vers la droite de 4 espaces exactement. Ce décalage s'appelle une **indentation** et il est obligatoire en Python pour constituer des blocs d'instructions.

- `def __init__(self, x, y, cote, tortue):` (là non plus ne pas oublier le `:`) c'est la ligne la plus pénible (d'un point de vue syntaxe), elle permet de définir une méthode mais ici, la méthode est particulière ; c'est la première qui sera invoquée automatiquement lors de la création de l'instance. Ca explique son nom bizarre, qu'il faut ***absolutement** respecter. Cette méthode est appellée **constructeur**. Dans les parenthèses nous mettons les informations dont nous aurons besoin lors de l'invocation de la méthode. La première information `self` laissons-la de côté, retenez simplement qu'il faudra **toujours** mettre `self` dans **toutes** les méthodes que vous définirez.

    Les autres informations sont, dans l'ordre l'abscisse et l'ordonnée du coin inférieur gauche, la taille du côté et la tortue qui va servir à dessiner le carré.

    La suite constitue le corps de la méthode et, comme pour la classe, ce corps est indenté de 4 espaces vers la droite.

- les 3 lignes suivantes permettent, par affectation de définir les propriétés `pos` (les coordonnées du coin inférieur gauche), `cote` (la dimension) et `t` la tortue.

Nous pouvons exécuter notre programme tel qu'il est actuellement, il ne fera juste rien : après pris connaissance de notre classe `Carre` l'interprète va simplement s'arrêter et nous _rendre la main_. Ajoutons les instructions pour :

1. créer un carré dont le coin inférieur sera en 0, 0, de taille 150px et de tortue une tortue anonyme
2. faisons se dessiner le carré

Lorsque votre code est écrit, exécutez le. S'il n'a pas provoqué d'erreur, votre fenêtre turtle a du s'ouvrir et se refermer à peine votre carré dessiné. C'est normal, votre programme s'est exécuter puis, une fois terminer l'interprète s'arrête et détruit ce qu'il a créé c'est-à-dire la fenêtre turtle.

Pour que la fenêtre reste ouverte, rajoutez comme **dernière** instruction de votre programme la ligne suivante :

```python
turtle.mainloop()
```

??? success "carre.py"
    
    ```python
    import turtle

    class Carre:

        def __init__(self, x, y, cote, tortue):
            self.pos = x, y
            self.cote = cote
            self.t = tortue
    
    
    # Le programme principal

    carre = Carre(0, 0, 150, turtle.Turtle())
    carre.t.fd(carre.cote)
    carre.t.lt(90)
    carre.t.fd(carre.cote)
    carre.t.lt(90)
    carre.t.fd(carre.cote)
    carre.t.lt(90)
    carre.t.fd(carre.cote)
    carre.t.lt(90)

    turtle.mainloop()
    ```

    Notez que le programme principal n'est pas indenté : il ne fait pas partie de la définition de notre classe.


## Définir des méthodes

Cela fait maintenant plusieurs fois que nous écrivons les huit instructions pour le dessin d'un carré. Cela commence à être fastidieux vous ne trouvez pas ? De plus nous répétons quatre fois avancer et tourner à gauche ; ce serait intéressant de pouvoir effectuer automatiquement cette répetition, **sans avoir à écrire explicitement les instructions**.

Nous allons définir une méthode `se_dessine()` qui n'a besoin d'aucune information supplémentaire. Toutes les informations sont des propriétés de l'objet et donc accessibles à l'objet. Par exemple `self.cote` permet d'obtenir la dimension du côté. Techniquement, et pour rappeler ce que nous avions vu à propos des variables, `self.cote` est une variable qui référence une valeur entière correspondant à la dimension du côté du carré dont il est question.

Notre méthode va ressembler à ceci :


```python
def se_dessine(self):
    # se positionner
    # répéter 4 fois 
        self.t.fd(self.cote)
        self.t.lt(90)
```

Voyons comment répéter des instructions. Rendez-vous au prochain chapitre pour voir comment réduire nos programmes en factorisant les choses.



