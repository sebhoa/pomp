# Premier objet

Il est temps pour nous de commencer à manipuler : Python et les _objets tortues_ (ne bondissez pas, nous ne parlons que de tortue informatique et personne ici ne considére l'animal comme un objet :smile:)

## Lancer un interprète Python et importer le module `turtle`

Dans la suite nous appelerons Terminal l'outil Terminal disponible sous les systèmes Linux ou Mac OSX ou l'outil Invite de commandes ou powershell sous le système Windows. De même, nous mettrons systématiquement `python` ou `pip` mais pour certains il faudra peut-être ajouter un 3 à la fin : `python3` `pip3`. 

Ouvrez donc un Terminal et tapez-y la commande suivante pour lancer l'interprète :

```bash
python
```

Et tout de suite, nous allons créer et manipuler une tortue :

``` python
>>> import turtle
>>>
``` 

## Première tortue

Pour créer une tortue nous faisons ceci :

```python
>>> turtle.Turtle()
>>>
```

Vous devriez avoir l'ouverture d'une fenêtre graphique similaire à :

![fenêtre turtle](../assets/images/fenetre_turtle.png)

??? warning "Mais également des messages pas sympas dans votre Terminal :"
    ``` bash
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    File "C:\Users\sebho\AppData\Local\Programs\Python\Python39\lib\turtle.py", line 3814, in __init__
        RawTurtle.__init__(self, Turtle._screen,
    File "C:\Users\sebho\AppData\Local\Programs\Python\Python39\lib\turtle.py", line 2558, in __init__
        self._update()
    File "C:\Users\sebho\AppData\Local\Programs\Python\Python39\lib\turtle.py", line 2661, in _update
        self._update_data()
    File "C:\Users\sebho\AppData\Local\Programs\Python\Python39\lib\turtle.py", line 2647, in _update_data
        self.screen._incrementudc()
    File "C:\Users\sebho\AppData\Local\Programs\Python\Python39\lib\turtle.py", line 1293, in _incrementudc
        raise Terminator
    turtle.Terminator
    ```

Pour fermer la fenêtre _turtle_ :

```python
>>> turtle.bye()
>>>
```

Puis quitter l'interprète (`Ctrl+Z` suivi de retour à la ligne) et relancer le tout.

Sans entrer dans les détails, ici nous avons créé un objet Turtle (nous distinguerons le module `turtle` de l'objet en mettant une majuscule) mais nous n'avions pas de moyen de communiquer avec lui. Pour pouvoir interagir avec notre tortue, nous allons lui donner un nom comme ceci :

```python
>>> caroline = turtle.Turtle()
>>>
```

Voilà qui est mieux : pas de messages d'erreurs et au centre de votre fenêtre une... flèche ?!! Oui nous y reviendrons.

![reference](../assets/images/reference.svg){align=right} Ainsi, nous avons créé un objet Turtle et lui avons donné un nom (on parle d'**identifiant**) par un mécanisme fondamental en programmation : l'**affectation** représenté par le symbole `=`. Ainsi, `caroline` est un identifiant nous permettant de garder un lien vers un objet Turtle. On dit que la **variable** `caroline` **référence** un objet Turtle. Le schéma ci-contre résume ce que nous venons de dire.



Nous pourrions même donner un surnom à Caroline... :

```python
>>> caro = caroline
>>>
```

![alias](../assets/images/reference2.svg){align=left} Grâce à l'affectation, nous allons référencer un objet par l'identifiant `caro`. Mais contrairement à notre première affectation, ici, à droite du symbole `=` il n'y a pas un objet mais une variable. Python va alors chercher (quelque part dans une zone que nous préciserons plus tard) s'il existe une variable nommée `caroline`. C'est bien le cas ici. Dès lors, Python va prendre comme objet l'objet référencé par cette variable. L'image ci-contre résume la nouvelle situation.

Passons au chapitre suivant pour commencer à interagir avec Caroline.




