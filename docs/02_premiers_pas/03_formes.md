# Dessiner

## Résumé des épisodes précédents

??? summary "previously on POMP..."
    Nous avons vu quelques méthodes pour :
    
    - changer la forme et la couleur de Caroline :
    ```python
    >>> caroline.shape('turtle')
    >>> caroline.color('green')
    ```
    - la faire avancer d'un nombre de pixels donnés :
    ```python
    >>> caroline.fd(100)
    ```
    - la faire pivoter sur elle-même à grauche ou à droite d'un angle donné en degré :
    ```python
    >>> caroline.lt(60)
    >>> caroline.rt(90)
    ```
    - la faire annuler sa dernière action :
    ```python
    >>> caroline.undo()
    ```
    - se réinitiliser presque totalement (la forme reste) :
    ```python
    >>> caroline.reset()
    ```

## Un premier carré

!!! question
    En utilisant les méthodes vues, faites dessiner à Caroline un carré de 150px de côté. Faites en sorte que Caroline regarde dans la même direction qu'avant d'avoir commencé son tracé.

??? success "Solution"
    Une solution possible (il y en a une autre en tournant vers la droite).
    
    ![carré](../assets/images/carre.png){align=right}
    ```python
    >>> caroline.fd(150)
    >>> caroline.lt(90)
    >>> caroline.fd(150)
    >>> caroline.lt(90)
    >>> caroline.fd(150)
    >>> caroline.lt(90)
    >>> caroline.fd(150)
    >>> caroline.lt(90)
    ```


## Attention à la position 

![carre centré](../assets/images/carre_centre.png){align=right} Nous souhaitons réaliser le dessin ci-contre (sans le point qui n'est là que pour matérialiser le centre du carré) où le centre du carré est la position initiale de Caroline c'est-à-dire le centre de la fenêtre graphique. Le carré fait, comme tout à l'heure 150px de côté.

!!! question
    Que vous manque-t-il ?

??? success "Solution" 
    pouvoir déplacer la tortue sans laisser de trace ; nous pouvons soulever le crayon de la tortue :
    
    ```python
    >>> caroline.up()
    ```

    Pour reposer le crayon :
    ```python
    >>> caroline.down()
    ```

??? success "Solution du carré"
    
    ```python
    >>> caroline.up()
    >>> caroline.lt(180)
    >>> caroline.fd(75)
    >>> caroline.lt(90)
    >>> caroline.fd(75)
    >>> caroline.lt(90)
    >>> caroline.down()
    >>> caroline.fd(150)
    >>> caroline.lt(90)
    >>> caroline.fd(150)
    >>> caroline.lt(90)
    >>> caroline.fd(150)
    >>> caroline.lt(90)
    >>> caroline.fd(150)
    >>> caroline.lt(90)
    ```

Nous constatons que le **programme** devient un peu long à écrire. En particulier à partir du moment où nous avons reposé le crayon (ligne 8) nous aimerions pouvoir réutiliser les instructions pour le dessin du carré. Ce dont nous avons besoin c'est de pouvoir définir notre propre objet `Carre`. Cet objet aurait les propriétés suivantes :

- `bas_gauche` : les coordonnées du coin inférieur gauche du carré
- `cote` : la taille en pixel du côté du carré
- `t` : la tortue associée permettant au carré de _se dessiner_

Passons au chapitre suivant pour apprendre à créer nos propres objets.

