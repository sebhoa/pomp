# Interactions

## Résumé des épisodes précédents

??? summary "previously on POMP..."
    Si jamais vous n'avez plus votre interprète ouvert ni votre fenêtre turtle, voici un résumé des étapes faites jusqu'ici :
    
    - Lancer l'interprète Python
    ```bash
    python
    ```
    Normalement `>>>` apparaissent vous indiaquant que vous êtes dans l'interprète interactif.
    
    - Importer le module `turtle`
    ```python
    >>> import turtle
    >>>
    ```
    - Créer une Turtle et lui donner un nom :
    ```python
    >>> caroline = turtle.Turtle()
    >>>
    ```


## Les méthodes d'un objet

Les méthodes sont les actions que l'objet peut effectuer. Demander à un objet d'effectuer une action c'est lui demander d'**invoquer** (on dit aussi **appeler**) une méthode. Pour cela nous devons nommer l'objet, nommer la méthode et fournir aussi les éventuelles informations dont la méthode pourrait avoir besoin.

Prenons le cas de Caroline. À sa naissance nous étions déçus de la voir avec une forme de... flèche. Nous pouvons demander à Caroline de changer sa forme. Pour cela, il faut lui faire invoquer la méthode `shape` (forme en anglais) en lui fournissant comme information le nom de la nouvelle forme, par exemple `'turtle'`

Profitons en pour lui demander de changer aussi sa couleur ; Caroline est une tortue verte :

```python
>>> caroline.shape('turtle')
>>> caroline.color('green')
>>>
```

Et nous voilà avec une bien jolie tortue : 

![caroline](../assets/images/caroline.png)

!!! Note "Syntaxe"
    pour qu'un objet invoque une méthode :
    ```python
    nom_objet.nom_methode(informations)
    ```


## Les propriétés d'un objet

La forme (`shape`) et la couleur (`color`) sont des caractéristiques des objets Turtle. En Informatique nous parlerons plutôt de **propriétés** ou d'**attributs**. Pour accéder à la propriété d'un objet il suffit de faire comme ceci :

!!! Note "Syntaxe"
    ```python
    nom_objet.une_propriete
    ```

Cette propriété est un peu comme une variable qui référence une valeur. Par exemple pour Caroline, `caroline.color` référence un objet couleur de valeur verte. Nous pourrions donc, en théorie faire ceci (et nous pourrons le faire effectivement avec nos propres objets, plus tard) :

```python
>>> caroline.color = 'red'
>>>
``` 

En réalité, les objets Turtle protègent leurs propriétés et nous pouvons y accèder **uniquement** par l'intermédiaire des méthodes disponibles. En Informatique ce concept s'appelle l'**encapsulation** et les méthodes accessibles sont l'**interface**. Laissons donc temporairement les propriétés, nous y reviendrons plus tard lorsque nous créerons nos propres objets.


## Faire bouger Caroline

Les objets Turtle peuvent bouger : avancer et tourner. Voici trois méthodes :

- `forward(d)` ou `fd(d)` : fait avancer la tortue qui invoque la méthode d'une distance de  _d_ pixels **dans la direction dans laquelle la tortue _regarde_**
- `left(a)` ou `lt(a)` : fait pivoter la tortue sur elle-même d'un angle de _a_ degré sur sa gauche
- `right(a)` ou `rt(a)` : fait pivoter la tortue sur elle-même d'un angle de _a_ degré sur sa droite

Faites avancer Caroline de 100px :

```python
>>> caroline.fd(100)
>>>
```

!!! question
    Que constatez-vous ?

??? success "Solution"
    Effectivement, la tortue laisse une trace que sol, de sa couleur :

    ![caroline avance](../assets/images/caroline_fd.png)

![rosace bleue](../assets/images/190_360.png){align=right} Nous allons demander à Caroline de réaliser pour nous quelques figures géométriques... simples pour commencer mais avec Berthe qui est une magnifique et très intelligente tortue bleue, voici ce que nous avons dessiné.

Avant de réaliser notre première forme géométrique, voici deux méthodes utiles pour effacer les éventuels loupés de Caroline : 

- `undo()` permet à l'objet Turtle d'annuler **sa dernière** action
- `reset()` réinitilise complètement la tortue (y compris sa couleur, mais pas sa forme)

Passons au chapitre suivant pour réaliser quelques formes géométriques.