# Python

![Logo Python](../assets/images/logo_python.svg){align=left} ![Guido van Rossum](../assets/images/guido_van_rossum.jpg){align=right} Le premier outil à installer est un _interprète Python3_ qui est le langage que nous allons utiliser pour cette initiation. [Python](https://www.python.org/) est un langage _multi-paragidmes_ (nous reviendrons sur ce terme) inventé par [Guido van Rossum](https://fr.wikipedia.org/wiki/Guido_van_Rossum), informaticien néerlandais.

Nous installerons aussi le module `turtle`.

## Installation de Python


=== "Linux / OSX"

    Rien de spécial à faire : python est nativement installé sur ces systèmes. Pour vérifier cela ouvrez un Terminal et taper la commande suivante :
    ``` bash
    python3
    ```

    Vous devriez obtenir quelque chose de similaire à :
    
    ```
    Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 26 2018, 23:26:24)
    [Clang 6.0 (clang-600.0.57)] on darwin
    Type "help", "copyright", "credits" or "licence" for more information.
    >>>
    ```

    Si ce n'est pas le cas, essayez la commande :
    ``` bash
    python
    ```
    
=== "Windows"

    Si vous avez déjà installé un environnement de type _conda_ ([miniconda](https://doc.ubuntu-fr.org/miniconda) ou [Anaconda](https://www.anaconda.com/products/individual)) il est probable que Python soit déjà installé. Référez-vous alors à votre environnement pour lancer un interpète Python.
    
    Dans le cas contraire, rendez-vous sur le site officiel de Python : [www.python.org](https://www.python.org) et suivez les instructions pour l'installation. 
    
    !!! warning "Attention" 
        Veuillez à bien cocher les deux petites cases en bas d'une des premières fenêtres de la procédure :

        ![cocher](../assets/images/install_python_windows.png)

!!! warning "Attention"
    Quelque soit votre système d'exploitation, votre version de Python doit être **3.x** et non 2.x ! Au moment où ce document est rédigé la dernière version est 3.9. Nous conseillons d'avoir au moins une version 3.6.


## Installation du module `turtle`

L'installation de modules se fera en utilisant l'outil `pip` si vous n'êtes pas sous un environnement _conda_ ou par l'outil `conda` sinon.

=== "sans conda"

    ```bash
    pip3 install turtle
    ``` 

    ou bien 

    ```bash
    pip install turtle
    ```

=== "avec conda"

    Il est possible que le module soit déjà installé. Dans une invite de commande lancer votre interprète Python :

    ``` bash
    python
    ```

    Une fois dans l'interprète (que vous reconnaissez aux 3 caractères `>` qui débute la ligne) :

    ```python
    >>> import turtle
    >>> help(turtle)
    ```

    Si vous obtenez l'aide sur le module c'est que tout est bien installé. Sinon, il faut faire une installation avec l'outil graphique de gestion des paquets de votre environnement ou alors via la commande suivante (à lancer dans une invite de commandes ou un powershell) :

    ``` bash
    conda install turtle
    ```

