# Jupyter

## Qu'est-ce que c'est

Il s'agit d'une application qui va vous permettre d'avoir un interprète Python **dans** votre navigateur et de pouvoir aussi y écrire du texte mis en forme.

??? example "Un exemple"
    ![exemple Notebook](../assets/images/exemple_notebook.png)

## Installation


=== "Windows environ Anaconda / Miniconda"

    Installer via votre environnement

=== "Windows sans conda"

    Vous pouvez essayer d'installer en tapant dans l'invite de commande (dans cmd) :

    ```bash
    pip install jupyterlab
    ```

    Si cela ne fonctionne pas vous devez prendre la procédure décrite dans le prochain chapitre.

=== "Linux / OSX"

    Comme Python est déjà installé sur vos machine, il suffit de lancer l'installation via `pip` :

    ```bash
    pip install jupyterlab
    ```

    !!! warning "Attention"
        `jupyterlab` est ici en **un seul mot**

###  Installation détaillée sous Windows

=== "Étape 1"

    Commencer par désintaller Python
    ![etape 00](../assets/images/00_clean.png)

=== "Étape 2"

    Sur le site officiel : http://python.org/downloads/ prenez la dernière version, en tout cas une version au moins égale à 3.6
    ![etape 02](../assets/images/install_01.png)

=== "Étape 3"

    !!! warning "Attention"
        Bien cocher la case `Add Python... to PATH`, puis cliquer sur `Install Now`

        ![etape 03](../assets/images/install_02.png)

=== "Étape 4"

    Installation réussie, vous pouvez fermer la fenêtre Steup 

    ![etape 04](../assets/images/install_03.png)

=== "Étape 5"

    Lancez `cmd` (appelé aussi Invite de commandes) : 

    ![etape 05](../assets/images/cmd_04.png)

=== "Étape 6"

    Lancez la commande pour l'installation de Jupyter : 

    ```bash
    pip install jupyterlab
    ```

    A la fin de l'installation il y a une étape optionnelle : mettre à jour pip

    ```bash
    pip install --upgrade pip
    ```

=== "Étape 7"

    Lancez jupyter :

    ```bash
    jupyter lab
    ``` 

    !!! warning "Attention"
        Cette fois il s'agit bien de **2 mots** : `jupyter` une espace `lab`.

    
    Vous devriez obtenir dans votre navigateur quelque chose de similaire à :

    ![jupyter](../assets/images/fenetre_jupyter.png)


## Quand utiliser Jupyter ?

- Jupyter servira essentiellement pour rédiger vos rapports.
- Pour lire les compte-rendus de séances
- Pour les énoncés de TP 

## Le langage Markdown

Il s'agit d'un langage de balisage très léger qui est utilisé dans les cellules de texte des notebooks jupyter mais pas que. Le fichier source qui a servi à créer la page web que vous lisez a été faite en markdown (md en abrégé). Voici les grandes lignes du markdown.

### Des Titres

!!! notation "Si on tape ceci..."
    ```md
    # Ceci est un titre de niveau 1
    ## Et là un niveau 2
    ### niveau 3
    ```

!!! tip "On obtient..."

    # Ceci est un titre de niveau 1
    ## Et là un niveau 2
    ### niveau 3


### Des emphases

!!! notation "Si on tape ceci..."
    ```md 
    On peut mettre du texte en **gras** comme ça aussi il est en __gras__
    Bref 2 * ou alors 2 _ 

    Sinon on peut aussi mettre *en italique* ou _comme ça_
    ```

!!! tip "On obtient..."
    On peut mettre du texte en **gras** comme ça aussi il est en __gras__
    Bref 2 * ou alors 2 _ 

    Sinon on peut aussi mettre *en italique* ou _comme ça_


### Des images

!!! notation "Si on tape ceci..."
    ```md 
    ![une rosace](../assets/images/2_360.png)
    ```

!!! tip "On obtient..."
    ![une rosace](../assets/images/2_360.png)



### Des liens

!!! notation "Si on tape ceci..."
    ```md 
    Pour vous entrainer vous pouvez faire [ce tutoriel en français](https://www.markdowntutorial.com/fr/)
    ```

!!! tip "On obtient..."
    Pour vous entrainer vous pouvez faire [ce tutoriel en français](https://www.markdowntutorial.com/fr/)


### Des formules mathématiques

!!! notation "Si on tape ceci..."
    ```md 
    $$\frac{\pi}{4} = \lim_{n\rightarrow\infty} \frac{\varphi(n)}{n^2}$$ 
    ```

!!! tip "On obtient..."
    $$\frac{\pi}{4} = \lim_{n\rightarrow\infty} \frac{\varphi(n)}{n^2}$$ 

### Des tableaux

!!! notation "Si on tape ceci...[^1]"
    ```md 
    | Langage | Inventeur        | Année |
    | :------ | :-------:        | ----: |
    | Python  | Guido van Rossum | 1991  |
    | Java    | James Gosling    | 1995  |
    | aligné à gauche | centré | aligné à droite |
    ```

[^1]: Le code du tableau n'a bien sûr pas besoin d'être bien aligné.

!!! tip "On obtient..."
    | Langage | Inventeur        | Année |
    | :------ | :-------:        | ----: |
    | Python  | Guido van Rossum | 1991  |
    | Java    | James Gosling    | 1995  |
    | aligné à gauche | centré | aligné à droite |


### Du code 

!!! notation "Si on tape ceci..."
    ``` 
       ```python
       def foo(n):
           return n**2
       ```
    ```


!!! tip "On obtient..."
    ```python
    def foo(n):
        return n**2
    ```

