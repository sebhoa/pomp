# Éditeur de texte

Un éditeur de texte va vous permettre de saisir vos codes Python, les sauvegarder dans un fichier pour pouvoir ensuite depuis un Terminal les exécuter.

On distingue de sorte d'éditeurs de texte :

1. Les éditeurs qui ne sont que des éditeurs, permettant de créer toute sorte de fichier texte ; on peut s'en servir pour créer des programmes Python mais aussi d'autres langages comme par exemple du html ou du css ; ou encore du javascript, du C etc.
2. Les IDE Python : ils embarquent aussi un interprète Python

## Les IDE

L'un des plus simples à installer et à utiliser est [Thonny](https://thonny.org/). Pour ceux qui on déjà installé l'interprète Python, Thonny peut s'installer via l'outil pip :

```bash
pip install thonny
```

Le lancement de l'IDE se fait aussi via le Terminal :

```bash
thonny
```

La fenêtre de dialogue du premier lancement permettant entre autre le choix de la langue :

![premier lancement Thonny](../assets/images/thonny.png)

La fenêtre de travail : en haut la partie éditeur pour écrire ses programmes, en bas l'interprète interactif :

![accueil Thonny](../assets/images/thonny2.png) 

## Les éditeurs

On peut opter pour un éditeur non couplé à un interprète. Les interprètes sont toutefois des outils puissants pour l'activité de programmation :

- raccourcis claviers et complétion 
- exécution des programmes sans sortir de l'éditeur
- lien avec des outils de versionning comme git
- etc.

Dans le cadre d'une utilisation de débutant l'éditeur servira à rédiger les codes et offrira quelques incontournables :

- indentation automatique
- colorisation syntaxique
- proposition de complétion

Voici une liste (loin d'être exhaustive) d'éditeurs connus :

- [VS Codium](https://vscodium.com/) une version Libre Open Source de [VSCode](https://code.visualstudio.com/)
- [Atom](https://atom.io/)
- [NotePadd++](https://notepad-plus-plus.org/) **windows**
- [gedit](https://doc.ubuntu-fr.org/gedit) **Ubuntu Linux**
- [kate](https://doc.ubuntu-fr.org/kate) **Ubuntu Linux**